#coding: utf-8
require 'spec_helper'

describe "Data uploader" do

  subject{ page}
  
  describe "New:" do

    upload_data="Cargar datos"
    cancel= "Cancelar"

    let(:valid_category){FactoryGirl.create(:category)}
    
    before do
      admin=FactoryGirl.create(:admin)
      sign_in_test admin
      visit new_datum_path
    end

    it{ should have_selector 'title', text: "Añadir datos"}
    it{ should have_selector 'h1', text: "Añade datos"}
    it{ should have_link cancel}
    
    describe "should have a form" do
      it{ should have_selector 'form'}
      it{ should have_selector 'label', text: "Categoría a la que pertenece"}
      it{ should have_selector 'label', text: "Datos"}
      it{ should have_selector 'input', type: 'submit', value: upload_data}
    end

    it "cancel button should back to index page" do
      click_link cancel
      current_path.should == data_path
    end
    
    describe "ogr2ogr from GDAL library must be installed" do
      standard_output=""

      before do
        standard_output= `ogr2ogr --version`
      end

      it{ standard_output.include? "GDAL".should be_true}
      it{ standard_output.include? "released".should be_true}
      
    end

    describe "after upload a valid file" do
      valid_data_path='spec/data_input/valid/'
      valid_data_files=['really_fast.json']#,'fast_test.json','enptr.zip', 'projected4326.json']

      valid_data_files.each do |valid_file| 

        before do
          5.times{ FactoryGirl.create(:category) }
          visit new_datum_path
          
          select(Category.first.name, :from => :category)
          attach_file :geojson, valid_data_path+valid_file

          click_button "Cargar datos"
        end
        
        it "[#{valid_file}] should redirect to show all upload data" do
          current_path.should == data_path
        end

        it "[#{valid_file}] should show a success message" do
          should have_selector('div.alert.alert-success', text: "correctamente")
        end

      end
      
      # describe "with invalid category" do

      #   before do
      
      #     visit new_datum_path
          
      #     select("invalid_category", from: :category)
      #     attach_file :geojson, valid_data_path+valid_data_files.first
        
      #     click_button "Cargar datos"
      #   end

      #   it "should render the upload page" do
      #     current_path.should == data_path
      #   end

      #   it "should show an error message" do
      #     should have_selector('div.alert.alert-error', text: "")
      #   end
      # end

    end


    describe "after upload an invalid file" do
      invalid_data_path='spec/data_input/invalid/'
      invalid_data_files=['lic.shp']#, 'uclm.png', 'cartografia.tar.gz', 'cartografia.tar']

      before do
        5.times{FactoryGirl.create(:category)}
        visit new_datum_path
      end

      invalid_data_files.each do |invalid_file|

        before do
          select(Category.first.name, from: :category)
          attach_file :geojson, invalid_data_path+invalid_file
          click_button "Cargar datos"
        end
        
        it "[#{invalid_file}] should render the upload page" do
          current_path.should == data_path 
        end

        it "[#{invalid_file}] should show an error message" do
          should have_selector('div.alert.alert-error', text: "")
        end

      end

    end
        
  end

  describe "Index: " do

    before do
      admin=FactoryGirl.create(:admin)
      sign_in_test admin
      5.times{ FactoryGirl.create(:datum)}
      visit data_path
    end
    
    it{ should have_selector 'title', text: "Datos guardados"}
    it{ should have_selector 'h1', text: "Datos guardados"}

    describe "should have a link to new_datum_path" do
      it{ should have_link "Añadir datos"}

      it "should redirect to new_datum_path" do
        click_on "Añadir datos"
        current_path.should == new_datum_path
      end
    end
    
    describe "remove all data" do
      before do
        number_of_datum=Datum.count

        number_of_datum.times do
          click_on "Eliminar datos"
        end
      end

      it{ should have_selector('div.alert.alert-success', text: "eliminado")}
      it{ should_not have_link "Eliminar datos"}
      it{ Datum.count.should == 0}
    end
    
  end

  describe "Permissions:" do
    
    let(:data_pages) do
      [
       data_path,
       new_datum_path
      ]
    end
    
    it "as user can't access" do
      user=FactoryGirl.create(:user)
      sign_in_test user

      data_pages.each do |page|
        visit page
        current_path.should == root_path
      end
      
    end

    it "as admin can access" do
      admin=FactoryGirl.create(:admin)
      sign_in_test admin

      data_pages.each do |page|
        visit page
        current_path.should == page
      end
    end
    
  end
  
end
