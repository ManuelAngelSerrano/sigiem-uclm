#coding: utf-8
require 'spec_helper'

describe "Devices" do

  subject{ page}

  describe "Access permissions" do
    
    before do
      device=create_device_with_sensors
      @paths=[devices_path, edit_device_path(device)]
    end

    it "ordinary users can't access" do
      user=FactoryGirl.create(:user)
      sign_in_test user

      @paths.each do |path|
        visit path
        current_path.should == root_path
      end
    end

    it "as admin can access" do
      admin=FactoryGirl.create(:admin)
      sign_in_test admin

      @paths.each do |path|
        visit path
        current_path.should == path
      end
    end

  end#access

  describe "Edit" do
    
    before do
      admin=FactoryGirl.create(:admin)
      sign_in_test admin
      
      @device=create_device_with_sensors

      visit edit_device_path(@device)
    end
    
    it{ should have_selector "title", text: "Configurar alertas"}
    it{ should have_selector "h1", text: "Configura las alertas"}
    it{ should have_selector "form"}

    describe "delete limits" do

      before do
        fill_in "Límite superior", with: ""
        fill_in "Límite inferior", with: ""
        click_on "Actualizar alertas"
      end
      
      it "should be invalid updated " do
        current_path.should == device_path(@device)
      end
    end

    describe "delete one limit (limit_sup)" do
      before do
        fill_in "Límite superior", with: ""
        click_on "Actualizar alertas"
      end
      
      it "should be a valid updated" do
        current_path.should == devices_path
      end

      it "alert should have limit_sup = nil" do
        @device.reload
        @device.sensors.first.alert.limit_sup.should == nil
      end
      
    end

    describe "edit limits" do

      before do
        @new_limit_inf=-100
        @new_limit_sup=100
        fill_in "Límite superior", with: @new_limit_sup
        fill_in "Límite inferior", with: @new_limit_inf
        click_on "Actualizar alertas"
      end

      it{ current_path.should==devices_path}
      
      it "should have new limits" do
        @device.reload
        @device.sensors[0].alert.limit_sup.should == @new_limit_sup
        @device.sensors[0].alert.limit_inf.should == @new_limit_inf
      end

    end

    
  end

  
  describe "Index" do

    before do

      admin=FactoryGirl.create(:admin)
      sign_in_test admin
      
      Device.remove
      Sensor.remove
      Alert.remove
      
      3.times do
        create_device_with_sensors
      end

      visit devices_path
    end

    it{ should have_selector "title", text: "Dispositivos"}

    it "destroying all data from devices should delete them from DB" do
      Device.count.times{ click_on "Eliminar datos"}
      
      Device.count.should==0
    end

    it "should have a link to add alert" do
      (Device.count-1).times{ click_on "Eliminar datos"}
      device=Device.first
      click_on "Configurar alertas"

      current_path.should== edit_device_path(device)
    end
    
  end # index
  



end
