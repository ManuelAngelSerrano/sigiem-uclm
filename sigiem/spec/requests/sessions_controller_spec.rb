#coding: utf-8
require 'spec_helper'

describe "Sessions" do
  subject{ page}
  
  describe "in /entrar:" do

    before do
      visit entrar_path
    end

    it{ should have_selector 'title', text:"Entrar"}
    it{ should have_content "Inicia sesión"}

    describe "should show a form" do
      it{ should have_selector "form"}
      it{ should have_selector "label", text: "Email"}
      it{ should have_selector "label", text: "Contraseña"}
      it{ should have_selector "input",value: "Entrar", type: "submit"}
    end

    it{ should have_link "crear una"}

    describe "signin with valid information," do
      before do
        @user= FactoryGirl.create(:user)
                
        fill_in "Email", with: @user.email
        fill_in "Contraseña", with: @user.password
        click_button "Entrar"
      end

      it "redirect to root page" do
        current_path.should == root_path
      end
      
      describe "elements for signed in users" do
        it{ should have_link "Salir"}
        it{ should_not have_link "Iniciar sesión"}

        it{ should have_content @user.name}
      end

      describe "user signed in can't access to" do
        describe "register page" do
          before do
            visit registrate_path
          end

          it{ current_path.should == root_path}
                    
          it "flash message of can't access " do
            should have_selector('div.alert.alert-notice', text: "ya está registrado")
          end
        end
        
        describe  "loggin page" do
          before{
            visit entrar_path
          }

          it{ current_path.should == root_path}
                    
          it "flash message of can't access " do
            should have_selector('div.alert.alert-notice', text: "Ya has iniciado sesión")
          end


        end
      end

      describe "and sign out" do
        before do
          click_link "Salir"
        end

        it{ should have_link "Iniciar sesión"}
        it{ should_not have_link "Salir"}
        it{ should_not have_content @user.name}

        it "flash message of sing out" do
          should have_selector('div.alert.alert-notice', text: "salido")
        end
        
      end
      
      
      it "flash message of success sign in" do
        should have_selector('div.alert.alert-success', text: "bienvenido")
      end
    end


    describe "signin with invalid information," do
      before do
        @user= FactoryGirl.create(:user)
                
        fill_in "Email", with: @user.email
        fill_in "Contraseña", with: @user.password+1.to_s
        click_button "Entrar"
      end

      it "redirect to login page (/entrar)" do
        current_path.should == entrar_path
      end
      
      it "elements for not signed in users" do
        should_not have_link "Salir"
        should have_link "Iniciar sesión"
      end

      it "flash message of error" do
        should have_selector('div.alert.alert-error', text: "incorrectos")
      end
      
    end

    
  end #/entrar

end
