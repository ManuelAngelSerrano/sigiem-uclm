#coding: utf-8

require 'spec_helper'

describe "WmsServers" do

  subject{ page}

  describe "Permissions: " do
    let(:actions_path) do
      actions_path=[wms_servers_path,
                    new_wms_server_path,
                    edit_wms_server_path(
                                         WmsServer.create(name: "server_test", url:"falsa.com")
                                         )
                   ]
    end

    it "as user can't access" do
      user= FactoryGirl.create(:user)
      sign_in_test user
      
      actions_path.each do |action|
        visit action
        current_path.should == root_path
      end
    end #as user

    
    it "as admin can access" do
      admin= FactoryGirl.create(:admin)
      sign_in_test admin
      
      actions_path.each do |action|
        visit action
        current_path.should == action
      end
    end #as admin
    
  end #permissions
  
  describe "Index:" do
    let(:number_of_servers){ 15}
    before do
      number_of_servers.times{ FactoryGirl.create(:wmsServer)}
      admin=FactoryGirl.create(:admin)
      sign_in_test admin
      visit wms_servers_path
    end

    it{ should have_selector 'title',text:"Servidores WMS"}
    it{ should have_selector 'h1', text: "Listado de servidores WMS"}
    it{ should have_selector 'small', text: @number_of_servers.to_s}
    
    it { should have_selector('div.pagination') }

    it "delete all servers" do
      number_of_servers.times do
        click_link "Eliminar servidor"
      end

      #visit wms_servers_path
      should have_selector 'small', text: "0"
    end

    it "whitout servers should show a message" do
      WmsServer.destroy_all
      visit wms_servers_path

      should have_content "Todavía no hay ningún servidor WMS"
    end

    describe "show flash message" do
      # it "error deleting server" do
      #   fake_server=WmsServer.new(name: "Fake Server", url:"fake_router.com")
      #   delete wms_server_path(fake_server), method: :delete
      
      #   should have_selector('div.alert.alert-error', text: "No se ha podido borrar ese servidor.")
      # end

      it "success deleting server" do
        click_link "Eliminar servidor"
        should have_selector('div.alert.alert-success', text: "Servidor borrado correctamente.")
      end
    end
    
    it "link to edit a server" do
      WmsServer.destroy_all
      temporal_server=FactoryGirl.create(:wmsServer)
      visit wms_servers_path
      click_link "Modificar datos"
      current_path.should == edit_wms_server_path(temporal_server)
    end

    it "link to add new servers" do
      should have_link "Añadir servidor WMS"
    end
    
  end #index


  describe "Edit:" do

    let(:update_server){ "Actualizar"}
    
    let(:temporal_server){
      FactoryGirl.create(:wmsServer)
    }
    
    before do
      admin=FactoryGirl.create(:admin)
      sign_in_test admin
      
      visit edit_wms_server_path(temporal_server)
    end

    it{ should have_selector 'title',text:"Editar servidor"}
    it{ should have_selector 'h1', text:"Actualiza los datos del servidor"}

    describe "should have a form" do
      it{ should have_selector 'form'}
      it{ should have_selector 'label', text: "Nombre"}
      it{ should have_selector 'label', text: "URL"}
      it{ should have_selector 'input', type: 'submit', value: :update_server}
    end

    it "should back to index page" do
      click_link "Cancelar"
      current_path.should == wms_servers_path
    end

    describe "with valid information" do
      let(:new_name){ "Another temporal server"}
      let(:new_url){ "another.url/temporal"}

      before do
        fill_in "Nombre", with: new_name
        fill_in "URL", with: new_url
        click_button :update_server
      end

      it{ should have_content new_name}
      it{ should have_content new_url}
      it{ should have_selector('div.alert.alert-success',
                               text: "Se han actualizado los datos correctamente.")}
      it{ current_path.should == wms_servers_path}
    end

    describe "with invalid information" do

      before do
        fill_in "Nombre", with: "1"
        fill_in "URL", with: "no"
        click_button :update_server
      end
      
      it "should show an error message" do
        should have_selector('div.alert.alert-error')
      end
      
      it "should render edit" do
        current_path.should == wms_server_path(temporal_server)
      end
      
    end
  end #edit

  describe "New:" do

    before do
      admin=FactoryGirl.create(:admin)
      sign_in_test admin

      visit new_wms_server_path
    end
    
    it{ should have_selector 'title', text: "Añadir WMS"}
    it{ should have_selector('h1', text: "Añade un servidor WMS")} 

    describe "should have a form" do
      it{ should have_selector 'form'}
      it{ should have_selector 'label', text: "Nombre"}
      it{ should have_selector 'label', text: "URL"}
      it{ should have_selector 'input', type: 'submit', value: "Añadir servidor"}
    end

    it "should back to index page" do
      click_link "Cancelar"
      current_path.should == wms_servers_path
    end

    describe "add server with valid information" do
      let(:new_name){ "New server"}
      let(:new_url){ "New.url.com/Toledo?"}
      
      before do
        fill_in "Nombre", with: new_name
        fill_in "URL", with: new_url
      end

      it "after add a server should show all servers" do
        click_button "Añadir servidor"
        current_path.should == wms_servers_path
      end
      
      it "should appear the name of the new server" do
        click_button "Añadir servidor"
        should have_content new_name
      end
    
      it "should appear the url of the new server" do
        click_button "Añadir servidor"
        should have_content new_url
      end
      
      it "should create a new server" do
        number_of_servers=WmsServer.count
        click_button "Añadir servidor"
        WmsServer.count.should == number_of_servers+1
      end

      it "should show a message" do
        click_button "Añadir servidor"
        should have_selector('div.alert.alert-success',
                               text: "Servidor creado correctamente.")
      end
    
    end

    describe "add server with invalid information" do

      let(:add_server){ "Añadir servidor"}
      
      let(:new_name){ "Valid name"}
      let(:new_url){ "AA"}
      
      before do
        fill_in "Nombre", with: new_name
        fill_in "URL", with: new_url
      end

      it "should render the same web" do
        click_button :add_server
        current_path.should == wms_servers_path
      end
        
      it "should not create a new server" do
        number_of_servers=WmsServer.count
        click_button :add_server
        WmsServer.count.should == number_of_servers
      end

      it "should show a message" do
        click_button :add_server
        should have_selector('div.alert.alert-error')
      end
      
    end #invalid information
    
  end #new
end
