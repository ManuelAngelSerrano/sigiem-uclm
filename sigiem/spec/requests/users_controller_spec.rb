#coding: utf-8
require 'spec_helper'

describe "User Controller," do
  
  subject{ page}
  
  describe "New user :" do
    
    before{ visit registrate_path}

    let(:registrar){ "Crear mi cuenta"}


    it{ should have_selector 'title', text: "Registro"}
    it{ should have_selector 'h1', text: "Regístrate"}
    it{ should have_link 'Iniciar sesión'}
    it{ should_not have_link 'Salir'}


    describe "should show a form" do

      it{ should have_selector 'form'}
      it{ should have_selector 'label', text: "Nombre"}
      it{ should have_selector 'label', text: "Email"}
      it{ should have_selector 'label', text: "Contraseña"}
      it{ should have_selector 'label', text: "Repita la contraseña"}

      it{ should have_selector 'input', type: 'submit', value: :registrar}
    end
    
    describe "with valid information," do
            
      before do
        fill_in "Nombre", with: "David"
        fill_in "Email", with: "david@dpzaba.com"
        fill_in "Contraseña", with: "dpzaba"
        fill_in "Repita la contraseña", with: "dpzaba"
      end

      it "user created (in BBDD)" do
        n1=User.count
        click_button :registrar
        n2=User.count
        
        n2.should eq n1+1
      end

      it "should show a message" do
        click_button :registrar
        should have_selector('div.alert.alert-success',
                             text: "Bienvenido")
      end

      it "user should be loged in" do
        click_button :registrar

        should have_link 'Salir'
        should_not have_link 'Iniciar sesión'
      end
      
    end #valid information

    describe "with invalid information" do

      #empty form
      it "should not create a user" do
        n1=User.count
        click_button :registrar
        n2=User.count
        n1.should eq n2
      end

      it "should show a message" do
        click_button :registrar
        should have_selector('div.alert.alert-error')
      end
      
      it "should render new" do
        click_button :registrar
        current_path.should == users_path
      end

      describe "user should not be logeed in" do
        it{ should_not have_link 'Salir'}
        it{ should have_link 'Iniciar sesión'}
      end

    end #invalid information
  end

  describe "Edit user: " do
     
    let(:actualizar){ "Actualizar mis datos"}
    let(:user){
      FactoryGirl.create(:user)
    }

    before do
      sign_in_test(user)
      visit edit_user_path(user)
    end
    
    it{ should have_selector 'title', text: "Perfil de usuario"}
    it{ should have_selector 'h1', text: "Edita"}

    links_for_signed_in_users
    
    it "content for signed in users" do
      content_for_signed_in_users(user)
    end

    
    describe "should show a form" do
      it{ should have_selector 'form'}
      it{ should have_selector 'label', text: "Nombre"}
      it{ should have_selector 'label', text: "Email"}
      it{ should have_selector 'label', text: "Contraseña"}
      it{ should have_selector 'label', text: "Repita la contraseña"}
      it{ should have_selector 'input', type: 'submit', value: :actualizar}
    end

    let(:new_email){ "david@uclm.es"}
    let(:new_password){ "www.uclm.es"}

    describe "with valid information" do

      before do
        fill_in "Email", with: new_email
        fill_in "Nombre", with: user.name
        fill_in "Contraseña", with: new_password
        fill_in "Repita la contraseña", with: new_password
        click_button  :actualizar
      end
      
      it{ user.reload.email.should == new_email}

      links_for_signed_in_users
      
      it "flash message of success update" do
        should have_selector('div.alert.alert-success', text: "correctamente")
      end

    end

    describe "with invalid information" do

      before do
        fill_in "Email", with: new_email
        fill_in "Nombre", with: user.name
        fill_in "Contraseña", with: user.password
        fill_in "Repita la contraseña", with: new_password
        click_button  :actualizar
      end

      links_for_signed_in_users

      it "flash message of success update" do
        should have_selector('div.alert.alert-error')
      end
    end

    describe "only him self can edit him information" do
      before do
        other_user=FactoryGirl.create(:user)
        visit edit_user_path(other_user)
      end

      it{ current_path.should == root_path}
    end
  end
 
  describe "Index:" do

    describe "as user" do
      let(:user){
          FactoryGirl.create(:user)
        }

      before do
        sign_in_test(user)
        visit users_path
      end

      it{ current_path.should == root_path}
    end

    describe "as administrator" do
      include UsersHelper

      let(:admin) do
        FactoryGirl.create(:admin)
      end
      
      before do
        sign_in_test admin
        visit users_path
      end
        
      links_for_signed_in_users
      it{ content_for_signed_in_users(admin)}

      it{ current_path.should == users_path}
      
      it{ should have_selector 'title', text: "Usuarios"}
      it{ should have_selector 'h1', text: "Listado de usuarios"}
      it{ should have_selector 'h4', text: spanish_role(admin.role)}

      it "show paginated users" do
        15.times{
          FactoryGirl.create(:user)
        }
        visit users_path
        should have_selector('div.pagination')
      end
             
      it "delete all users" do
        3.times{
          FactoryGirl.create(:user)
        }
        visit users_path
        User.find_all_by_role(:user).count.times do
          click_link "Eliminar"
        end
        User.find_all_by_role(:user).count.should == 0
      end

      it "delete all admins" do
        3.times{
          FactoryGirl.create(:admin)
        }
        visit users_path
        (User.find_all_by_role(:admin).count-1).times do
          click_link "Eliminar"
        end
        
        User.find_all_by_role(:admin).count.should == 1 #admin who delete
      end
      
      describe "changing role:" do
        before do
          3.times{
            FactoryGirl.create(:user)
            FactoryGirl.create(:admin)
          }
          visit users_path
        end
        it "change all users to admin" do
          User.find_all_by_role(:user).count.times do
            click_link "Convertir a Administrador"
          end
          User.find_all_by_role(:user).count.should == 0
        end

        it "change all admins to users" do
          (User.find_all_by_role(:admin).count-1).times do
            click_link "Convertir a Usuario"
          end
          User.find_all_by_role(:admin).count.should == 1 #admin who change
        end
      end #changing role
      
    end # as admin

  end #index
  
end



