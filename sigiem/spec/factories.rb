# when you modify this document, restart guard (test)
FactoryGirl.define do
  factory :user do
    
    sequence(:name){ |n| "User Test #{n}"}
    sequence(:email){ |n| "user_#{n}@user.com"}
    password "daviddavid"
    password_confirmation "daviddavid"

    factory :admin do
      role :admin
    end

    factory :super_admin do
      role :super_admin
    end
  end
  
  factory :wmsServer do
    sequence(:name){ |n| "server test #{n}"}
    sequence(:url){ |n| "url_Test.com/getMap?#{n}"}
  end

  factory :category do
    sequence(:name){ |n| "category test #{n}"}
    sequence(:parent){ |n| ""}
  end

  factory :datum do |dat|
    sequence(:name) do |n|
      "Datum_#{n}.zip"
    end
    sequence(:geojson) do |n|
      {"type"=>"Feature",
        "properties"=>{
          "Test_propertie #{n}"=>"Decreto 161/02, de 12 de Noviembre",
          "FIGURA"=>"MONUMENTO NATURAL",
          "NOMBRE"=>"SIERRA DE PELA Y LAGUNA DE SOMOLINOS",
          "CODIGO"=>"ENP_040"},
        "geometry"=>{
          "type"=>"Polygon",
          "coordinates"=>[[[n, n], [n+1, n+1], [n+2, n+2], [n-3.061181,n+41.272136], [DateTime.now.sec, DateTime.now.sec**2],   [DateTime.now.sec, DateTime.now.sec**2],  [DateTime.now.sec, DateTime.now.sec**2],  [DateTime.now.sec, DateTime.now.sec**2],  [DateTime.now.sec, DateTime.now.sec**2] ]]
        }
      }
    end
    category
  end

  factory :alert do
    sequence(:limit_inf){ |n| n}
    sequence(:limit_sup){ |n| n*2}
    record { [] }
    index_last_value { 0 }
  end
  
  factory :sensor do |sens|
    sequence(:name){ |n| "sensor test #{n+1}"}
    sequence(:type){ |n| "type#{n+1}".to_sym}
    sequence(:values){ |n| (n..n**2).to_a}
    alert { FactoryGirl.build(:alert)}
  end
  
  factory :device do
    sequence(:name){ |n| "device test #{n+1}"}
    lat {Random.new.rand(-90..90)}
    long {Random.new.rand(-180..180)}
    
    sensors {
      [FactoryGirl.build(:sensor)]
    }
  end

end







