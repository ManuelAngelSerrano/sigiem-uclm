require 'spec_helper'

describe Category do

  before do
    @category=FactoryGirl.create(:category)
  end

  subject{ @category}
  
  it{ should be_valid}
  
  describe "should respond to" do
    it { should respond_to :name}
    it { should respond_to :parent}
  end
  
  describe "name:" do

    it "is too long" do
      @category.name="a"*31
      
      should_not be_valid
    end

    it "is duplicate in DB (isn't case sensitive)" do
      category_duplicate=@category.dup
      category_duplicate.name.upcase!

      category_duplicate.should_not be_valid
    end

    it "is duplicate in DB" do
      category_duplicate=@category.dup

      category_duplicate.should_not be_valid
    end
  end #name

  describe "could have a datum associated (one)" do

    before do
      datum=FactoryGirl.create(:datum)
      datum.category=@category
      @category.datum=datum
    end

    it "and each datum should ve valid" do
      @category.datum.should be_valid
    end

  end
  
end
