require 'spec_helper'

describe Alert do

  before do
    @alert=FactoryGirl.build(:alert)
    device=FactoryGirl.build(:device)
    sensor=FactoryGirl.build(:sensor)
    sensor.device=device
    device.sensors << sensor
    sensor.alert=@alert
    @alert.sensor=sensor

    @alert.save
    device.save
  end

  
  subject{ @alert}
  
  it{ should be_valid}

  it "saving it should create his associated objects  (check factories are ok)" do
    Alert.count.should == 1
    Sensor.count.should == 1
    Device.count.should == 1
  end
  
  describe "should respond to" do
    it { should respond_to :limit_sup}
    it { should respond_to :limit_inf}
    it { should respond_to :record}
    it { should respond_to :index_last_value}
    
    it { should respond_to :sensor}
  end

  it "should not have a nil sensor associated" do
    @alert.sensor.should_not == nil
  end

  describe "limits should have a valid format," do

    it "without limits" do
      @alert.limit_inf=nil
      @alert.limit_sup=nil

      should_not be_valid
    end

    it "with just one valid limit" do
      @alert.limit_inf=nil

      should be_valid
    end

    it "with one valid limit and other one invalid" do
      @alert.limit_inf=100
      @alert.limit_sup="Invalid_value"
      #invalid value should be nil

      should be_valid
    end
    
  end

  it "should swap limits if limit_sup is lower than limit_inf" do
    @alert.limit_inf=100
    @alert.limit_sup=1
    @alert.save
    
    (@alert.limit_sup).should >= (@alert.limit_inf)
  end

  describe "check record" do

    it "allways should not have a nil record, should be empty" do
      @alert.record=nil
      @alert.record.should == []
    end
    
    it "index_last_value should be lower or equals than the length of values of his associated sensor" do
      @alert.check_values_of_sensor
      @alert.index_last_value.should <= @alert.sensor.values.length
    end
    
    it "should add alerts to her record" do
      @alert.sensor.values.clear
      @alert.sensor.values << @alert.limit_sup+1
      @alert.sensor.values << @alert.limit_inf-1
      @alert.check_values_of_sensor
      
      @alert.record.length.should == 2
    end

    
    it "should not add alerts to her record" do
      @alert.sensor.values.clear
      @alert.sensor.values << @alert.limit_sup
      @alert.sensor.values << Random.new.rand(@alert.limit_inf..@alert.limit_sup)
      @alert.sensor.values << @alert.limit_inf
      @alert.check_values_of_sensor
      
      @alert.record.length.should == 0
    end
  end #record
  
end
