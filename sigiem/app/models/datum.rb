#coding: utf-8
class Datum
  include MongoMapper::Document
  safe
  
  attr_accessible :geojson, :name

  key :name, String, required: true
  key :geojson, Hash, required: true
  
  belongs_to :category
  
  validates_uniqueness_of :category_id
  validates_presence_of :category_id
  
  validate :correct_geojson_format

  private

  # Specification in http://geojson.org/geojson-spec.html#geojson-objects
  def correct_geojson_format

    valid_geojson_types=["Point", "LineString", "Polygon", "MultiPoint",
                         "MultiLineString", "MultiPolygon", "GeometryCollection",
                        "Feature", "FeatureCollection"]
    spanish_error="deben tener un formato correcto"
    
    if self.geojson["type"].nil?
      #must have one type field
      errors.add(:geojson, spanish_error)
    else

      if not valid_geojson_types.include?(self.geojson["type"])
        #must have one type field with one of this values +valid_geojson_types.to_s
        errors.add(:geojson, spanish_error)
      end
      
    end
    
  end
  
end
