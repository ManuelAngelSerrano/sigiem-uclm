#coding: utf-8
class Device
  include MongoMapper::Document
  safe

  attr_accessible :name, :lat, :long, :sensors
  
  key :name, String, required: true, unique: true, :case_sensitive => false
  key :lat, Float, required: true
  key :long, Float, required: true

  many :sensors, :dependent => :destroy

  validates_presence_of :sensors
  before_save :downcase_properties
  validate :format_coordinates
  

  private

  def downcase_properties
    self.name.downcase!
  end
  
  def format_coordinates
    valid_lat=[-90, 90]
    valid_long=[-180, 180]

    if self.lat.nil? or self.long.nil?
      return
    end
    
    if not is_between?(self.lat,valid_lat) or not is_between?(self.long, valid_long)
      errors.add(:format_coordinates, "rango inválido de latitud: (-90º a 90º) o longitud (-180º a 180º)")
    end
    
  end

  def is_between? number, array_values

    if number >= array_values[0] and number <= array_values[1]
      return true
    end
    
    return false
  end
  
end
