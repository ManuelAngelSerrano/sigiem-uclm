#coding: utf-8
require 'zip/zipfilesystem'
require 'json'

class DataController < ApplicationController

  before_filter :signed_in
  before_filter :signed_in_as_admin

  
  def new
    @datum=Datum.new
  end

  def create

    @datum=process_file params
    
    if not @datum.save
      render 'new'
    else
      flash[:success]="Datos de #{@datum.name} añadidos correctamente a #{@datum.category.name}."
      redirect_to data_path
    end
    
  end

  def index
    @data=Datum.fields(:name, :category_id).all
  end

  def destroy
    Datum.find_by_id(params[:id]).destroy
    flash[:success]="Archivo de datos eliminado correctamente."
    redirect_to data_path
  end


  private

  Upload_dir='upload/'
  Sufix_uncompressed_dir="_dir"
  Sufix_result_file="_result.json"


  def process_file params
    @datum=Datum.new

    if params[:datum][:geojson].nil?
      return @datum
    end
    
    # ActionDispatch::Http::UploadedFile
    # zip (shp), geojson, kml, gml, ...
    file_uploaded=params[:datum][:geojson] 
    file_uploaded_name=file_uploaded.original_filename

    copy_path=copy_and_extract(file_uploaded)

    format_result = change_format copy_path[:extract_path], file_uploaded_name

    remove_upload_file copy_path

    if format_result[:correct]
      
      data=File.open(format_result[:result_file])
      data=data.read
      data=data.force_encoding("UTF-8")
      data=JSON.parse(data)

      #data=simplify_data(data)

      remove_file format_result[:result_file]

      datum_category=Category.find_by_name(params[:datum][:category].downcase)
      @datum=Datum.new(geojson: data, name: file_uploaded_name)
      @datum.category=datum_category
      
    else
      @datum.errors.add(:geojson,"No se ha podido procesar o descomprimir el archivo '#{file_uploaded_name}', compruebe que tiene un formato aceptado y sea válido.")
      flash.now[:error]="No se ha podido procesar o descomprimir el archivo '#{file_uploaded_name}', compruebe que tiene un formato aceptado y sea válido."
      #puts @datum.errors.messages
    end
    
    return @datum
  end
  
  
  def copy_and_extract file_uploaded
    file_path=Upload_dir+file_uploaded.original_filename

    #copy to Upload_dir
    File.open(file_path, 'wb') do |file|
      file.write(file_uploaded.read)
    end

    extract_path=""
    root_extract_path=""
    if file_uploaded.content_type.include? "application/zip" or
        file_uploaded.content_type.include? "application/x-zip-compressed"
      
      root_extract_path=file_path+Sufix_uncompressed_dir

      begin
        Zip::ZipFile.open(file_path) do |zip_file|
          zip_file.each do |f|
            f_path=File.join(root_extract_path, f.name)
            extract_path=FileUtils.mkdir_p(File.dirname(f_path))
            zip_file.extract(f, f_path) unless File.exist?(f_path)
          end
        end
      rescue Exception
      end
    end

    compressed=false
    #not compressed
    if extract_path.empty?
      extract_path=file_path
    else
      compressed=true
      extract_path=extract_path.first
    end

    return {file_path: file_path, extract_path: extract_path,
      root_extract_path: root_extract_path, compressed: compressed}
  end

  
  def change_format file_path, file_name
    
    new_file_name=file_name.split(".")[0]+Sufix_result_file
    new_file_path=Upload_dir

    new_file=new_file_path+new_file_name
    
    file=file_path

    #simplify with: -simplify 800
    options="-f 'GeoJSON'  #{new_file} #{file} -t_srs \"EPSG:4326\" -lco ENCODING=UTF-8"
    standard_output=`ogr2ogr #{options} 2>&1`

    correct=$?.success?
    
    return {correct: correct, result_file: new_file}
  end
  

  def remove_upload_file copy_file_path

    remove_file copy_file_path[:file_path]
    
    if copy_file_path[:compressed]
      remove_file copy_file_path[:root_extract_path]
    end

  end

  def remove_file file_path
    FileUtils.rm_r(file_path, secure: true)
  end

  
end
