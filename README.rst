=====================================
`SIGIEM <http://projects.dpzaba.com/sigiem>`_ 
=====================================

Online Geographical Information System with real time data for `Castile-La Mancha <https://en.wikipedia.org/wiki/Castile-La_Mancha>`_.

Try it in `sigiem.uclm.es <http://sigiem.uclm.es>`_ (it isn't the last version)

Features::

- Add new data to display (it supports a lot of GIS standards, thanks to OGR).
- Receive new data from sensors (API).
- Display alerts of sensors in real time.
- Manage users, data categories and alerts.
- Sensors emulator.
- Script to deploy with data example.


Used tools::

  1-  Ruby (v 1.9.1)
  2-  Rails (v 3.2.9)
  3-  MongoDB (MongoMapper)
  4-  BootStrap 
  5-  Javascript and JQuery (Ajax)
  6-  GNU/Linux
  7-  Emacs
  8-  Git
  9-  Rspec (2.10.0)
  10- Capybara (1.1.2)
  11- Leaflet
  12- Apache + Passenger
  13- I18n (spanish translation)
  14- Spork and Guard (continuous integration)
  15- GDAL (Geospatial Data Abstraction Library) and OGR
  16- SimpleCov (code coverage analysis tool for Ruby 1.9)
  17- Maps from OpenStreetMap provided by CloudMade


Some stats (12 Jan 2013)::

-  Code LOC: 849     Test LOC: 1640     Code to Test Ratio: 1:1.9
-  Number of tests: 325
-  Test coverage: 91.11%

`David Antonio Perez Zaba <http://www.dpzaba.com>`_
======================================================================================================




